function colors = mangaspecer(N)
% My own version of linspecer.
% N: for compability with linspecer... Not used for anything...

% colors = get(gca,'colororder'); % new MATLAB colors
% 
% colors(3, :) = colors(5, :); % green should be the third color...

% old MATLAB colors
colors = [0, 0, 1; % yeah! pure blue
          1, 0, 0;
          0, 1, 0]; % pure green! don't care if it is too bright for your eyes!


end
function fig2pdf()

h = findall(0, 'type', 'figure');%get(0, 'children');

filesNames = '';
for i=1:length(h)
    figure(i);
    filenameEps = ['figure', num2str(i), '.eps'];
    filenamePdf = ['figure', num2str(i), '.pdf'];
    pause(0.1);
    print('-depsc2', filenameEps);
    system(['epstopdf ', filenameEps]);
    filesNames = [filesNames, ' ', filenamePdf];
end

system(['pdfunite', filesNames, ' figures.pdf']);

for i=1:length(h)
    filenameEps = ['figure', num2str(i), '.eps'];
    filenamePdf = ['figure', num2str(i), '.pdf'];
    system(['rm ', filenameEps]);
    system(['rm ', filenamePdf]);
end

end
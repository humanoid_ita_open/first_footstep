function value = saturate(value, minimum, maximum)
    if value < minimum
        value = minimum;
    end
    if value > maximum
        value = maximum;
    end
end
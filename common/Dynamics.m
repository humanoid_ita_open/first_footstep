classdef Dynamics < handle
    properties
        A;
        B;
        C;
    end
    
    methods
        function self = Dynamics(A, B, C)
            self.A = A;
            self.B = B;
            self.C = C;
        end
        
        function x = step(self, x, u)
            x = self.A * x + self.B * u;
        end
        
        function y = output(self, x)
            y = self.C * x;
        end
    end
end
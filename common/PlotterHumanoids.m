classdef PlotterHumanoids < handle
    methods(Static)
        function plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, theta,...
                prediction, x, y, T, N, footDimensions)
            lineWidth = 2;
            fontSize = 14;
            
            times = 0:T:(N-1)*T;
            X = prediction.Pps * x + prediction.Ppu * d3X;
            Y = prediction.Pps * y + prediction.Ppu * d3Y;
            dX = prediction.Pvs * x + prediction.Pvu * d3X;
            dY = prediction.Pvs * y + prediction.Pvu * d3Y;
            d2X = prediction.Pas * x + prediction.Pau * d3X;
            d2Y = prediction.Pas * y + prediction.Pau * d3Y;
            Zx = prediction.Pzs * x + prediction.Pzu * d3X;
            Zy = prediction.Pzs * y + prediction.Pzu * d3Y;
            figure;
            hold on;
            plot(times, d3X, 'LineWidth', lineWidth);
            plot(times, d3Y, 'r', 'LineWidth', lineWidth);
            xlabel('Time [$s$]', 'FontSize', fontSize, 'interpreter', 'latex');
            ylabel('Jerk [$m/s^3$]', 'FontSize', fontSize, 'interpreter', 'latex');
            legend({'X Jerk', 'Y Jerk'}, 'FontSize', fontSize, 'interpreter', 'latex');
            grid on;
            set(gca, 'FontSize', fontSize);
            set(gca, 'ticklabelinterpreter', 'latex');
            figure;
            hold on;
            plot(times, dX, 'LineWidth', lineWidth);
            plot(times, dY, 'r', 'LineWidth', lineWidth);
            xlabel('Time [$s$]', 'FontSize', fontSize, 'interpreter', 'latex');
            ylabel('Speed [$m/s$]', 'FontSize', fontSize, 'interpreter', 'latex');
            legend({'X Speed', 'Y Speed'}, 'FontSize', fontSize, 'interpreter', 'latex');
            grid on;
            set(gca, 'FontSize', fontSize);
            set(gca, 'ticklabelinterpreter', 'latex');
            figure;
            hold on;
            plot(times, d2X, 'LineWidth', lineWidth);
            plot(times, d2Y, 'r', 'LineWidth', lineWidth);
            xlabel('Time [s]', 'FontSize', fontSize, 'interpreter', 'latex');
            ylabel('Acceleration [$m/s^2$]', 'FontSize', fontSize, 'interpreter', 'latex');
            legend({'X Acceleration', 'Y Acceleration'}, 'FontSize', fontSize, 'interpreter', 'latex');
            grid on;
            set(gca, 'FontSize', fontSize);
            figure;
            hold on;
            plot(times, Xfaux, 'LineWidth', lineWidth);
            plot(times, Yfaux, 'r', 'LineWidth', lineWidth);
            plot(times, Zx, '--', 'LineWidth', lineWidth);
            plot(times, Zy, 'r--', 'LineWidth', lineWidth);
            xlabel('Time [s]', 'FontSize', fontSize);
            ylabel('Foot Position [m]', 'FontSize', fontSize);
            legend('X Foot Position', 'Y Foot Position');
            grid on;
            set(gca, 'FontSize', fontSize);
            
            figure;
            hold on;
%             numSteps = size(Xfaux, 1);
            numSteps = size(Xfb, 1);
            length = footDimensions.length;
            width = footDimensions.width;
            for s=1:numSteps
                psi = theta(s);
                center = [Xfb(s); Yfb(s)];
%                 center = [Xfaux(s); Yfaux(s)];
                R = rotationMatrix(psi);
                topLeft = center + R * [-length / 2; width / 2];
                topRight = center + R * [length / 2; width / 2];
                bottomLeft = center + R * [-length / 2; -width / 2];
                bottomRight = center + R * [length / 2; -width / 2];
                footContourX = [topLeft(1), topRight(1), bottomRight(1),...
                    bottomLeft(1), topLeft(1)];
                footContourY = [topLeft(2), topRight(2), bottomRight(2),...
                    bottomLeft(2), topLeft(2)];
                plot(footContourX, footContourY, 'k', 'LineWidth', lineWidth);
            end
            pxN = prediction.Pps(end, :) * x + prediction.Ppu(end, :) * d3X;
            pyN = prediction.Pps(end, :) * y + prediction.Ppu(end, :) * d3Y;
            vxN = prediction.Pvs(end, :) * x + prediction.Pvu(end, :) * d3X;
            vyN = prediction.Pvs(end, :) * y + prediction.Pvu(end, :) * d3Y;
            axN = prediction.Pas(end, :) * x + prediction.Pau(end, :) * d3X;
            ayN = prediction.Pas(end, :) * y + prediction.Pau(end, :) * d3Y;
            xN = [pxN; vxN; axN];
            yN = [pyN; vyN; ayN];
            lambda = sqrt(Constants.GRAVITY / prediction.dynamics.zCOM);
            alphaX = pxN + vxN / lambda;
            gammaX = -vxN / lambda;
            alphaY = pyN + vyN / lambda;
            gammaY = -vyN / lambda;
            T = 0.01;
            Xfuture = zeros(100, 1);
            Yfuture = zeros(100, 1);
            for i=1:size(Xfuture, 1)
                Xfuture(i) = alphaX + gammaX * exp(-lambda * i * T);
                Yfuture(i) = alphaY + gammaY * exp(-lambda * i * T);
            end
            axis equal
            grid on
            h1 = plot(X, Y, 'b', 'LineWidth', lineWidth);
            h2 = plot(Zx, Zy, 'r', 'LineWidth', lineWidth);
            h3 = plot(Xfuture, Yfuture, 'b--');
            h4 = plot([1, 1 / lambda, 0] * xN, [1, 1 / lambda, 0] * yN, 'bx', 'MarkerSize', 10);
            xlabel('X ($m$)', 'FontSize', fontSize, 'interpreter', 'latex')
            ylabel('Y ($m$)', 'FontSize', fontSize, 'interpreter', 'latex')
            legend([h1 h2], {'CoM', 'ZMP'}, 'FontSize', fontSize, 'interpreter', 'latex','Location','southwest')
            set(gca, 'FontSize', fontSize);
            set(gca, 'ticklabelinterpreter', 'latex');
        end
    end
end

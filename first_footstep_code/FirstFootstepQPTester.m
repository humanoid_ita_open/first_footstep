classdef FirstFootstepQPTester < handle
    properties
    end
    
    methods(Static)
        function testTimestepToFootstep()
            N = 10;
            Nstep = 5;
            footsteps = zeros(1, N);
            for m=1:N
                for i=1:N
                    j = FirstFootstepQPOptimizerFactory.timestepToFootstep(i, m, Nstep);
                    footsteps(i) = j;
                end
                figure;
                title(sprintf('m = %d', m));
                plot(1:N, footsteps);
                xlabel('Timestep');
                ylabel('Footstep');
            end
        end
        
        function testJerkCost()
            alpha = 10^-6;
            beta = 1;
            N = 10;
            m = 5;
            Nstep = 5;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = ones(N, 1);
            dYr = ones(N, 1);
            
            variables = FirstFootstepQPOptimizerFactory.makeVariables(N, Nstep, m);
            
            variablesCell = FirstFootstepQPOptimizerFactory.makeVariablesCell(variables);
            
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.kprime = sdpvar;
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.x,...
                params.y
                };
            
            paramsValues = {
                dXr,...
                dYr,...
                x,...
                y
                };
            
            jerkCost = FirstFootstepQPOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = FirstFootstepQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            
            cost = jerkCost + velTrackCost;
            
            constraints = [];
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            times = zeros(1, 100);
            for i=1:100
                tic
                solution = problem(paramsValues);
                times(i) = toc;
            end
            
            figure;
            plot(times, 'LineWidth', 2);
            title(sprintf('mean %g std %g', mean(times), std(times)));
            xlabel('Iteration (-)', 'FontSize', 14);
            ylabel('Solver Time (s)', 'FontSize', 14);
            set(gca, 'FontSize', 14);
            
            d3X = solution{1};
            Xf = solution{2};
            d3Y = solution{3};
            Yf = solution{4};
            U = FirstFootstepQPOptimizerFactory.makeFootstepSelectMatrix(N, Nstep, m);
            Xfaux = U * Xf;
            Yfaux = U * Yf;
            
            theta = zeros(N, 1);
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xf, Yf, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testZMPSupportPolygon()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            m = 5;
            Nstep = 5;
            Nf = FirstFootstepQPOptimizerFactory.getNumSteps(N, Nstep, m);
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            xc = 0;
            yc = 0;
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = ones(N, 1);
            dYr = ones(N, 1);
            theta = zeros(Nf, 1);
            cf = cos(theta);
            sf = sin(theta);
            
            variables = FirstFootstepQPOptimizerFactory.makeVariables(N, Nstep, m);
            
            variablesCell = FirstFootstepQPOptimizerFactory.makeVariablesCell(variables);
            
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            params.xc = sdpvar;
            params.yc = sdpvar;
            params.cf = sdpvar(Nf, 1);
            params.sf = sdpvar(Nf, 1);
            params.Nf = Nf;
            
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.x,...
                params.y,...
                params.xc,...
                params.yc,...
                params.cf,...
                params.sf
                };
            
            paramsValues = {
                dXr,...
                dYr,...
                x,...
                y,...
                xc,...
                yc,...
                cf,...
                sf
                };
            
            [dxsp, dysp, bsp] = FirstFootstepQPOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
            
            jerkCost = FirstFootstepQPOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = FirstFootstepQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            zmpTrackCost = FirstFootstepQPOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma, m);
            
            cost = jerkCost + velTrackCost + zmpTrackCost;
            
            zmpErrorConst = FirstFootstepQPOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp, m);
            currStepConst = FirstFootstepQPOptimizerFactory.makeCurrentStepConstraints(variables, params);
            boundConst = FirstFootstepQPOptimizerFactory.makeBoundConstraints(variables);
            
            constraints = zmpErrorConst +...
                currStepConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            times = zeros(1, 100);
            for i=1:100
                tic
                solution = problem(paramsValues);
                times(i) = toc;
            end
            
            figure;
            plot(times, 'LineWidth', 2);
            title(sprintf('mean %g std %g', mean(times), std(times)));
            xlabel('Iteration (-)', 'FontSize', 14);
            ylabel('Solver Time (s)', 'FontSize', 14);
            set(gca, 'FontSize', 14);
            
            d3X = solution{1};
            Xf = solution{2};
            d3Y = solution{3};
            Yf = solution{4};
            U = FirstFootstepQPOptimizerFactory.makeFootstepSelectMatrix(N, Nstep, m);
            Xfaux = U * Xf;
            Yfaux = U * Yf;
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xf, Yf, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testZMPFeetCollision()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            m = 4;
            Nstep = 5;
            Nf = FirstFootstepQPOptimizerFactory.getNumSteps(N, Nstep, m);
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            xc = 0;
            yc = 0;
            bls = 1;
            ySep = 0.4;
            
            theta = zeros(Nf, 1);
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = ones(N, 1);
            dYr = zeros(N, 1);
            cf = cos(theta);
            sf = sin(theta);
            
            variables = FirstFootstepQPOptimizerFactory.makeVariables(N, Nstep, m);
            
            variablesCell = FirstFootstepQPOptimizerFactory.makeVariablesCell(variables);
            
            params.N = N;
            params.Nstep = Nstep;
            params.Nf = Nf;
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            params.xc = sdpvar;
            params.yc = sdpvar;
            params.bls = sdpvar;
            params.cf = sdpvar(Nf, 1);
            params.sf = sdpvar(Nf, 1);
            
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.x,...
                params.y,...
                params.xc,...
                params.yc,...
                params.bls,...
                params.cf,...
                params.sf
                };
            
            paramsValues = {
                dXr,...
                dYr,...
                x,...
                y,...
                xc,...
                yc,...
                bls,...
                cf,...
                sf
                };
            
            [dxfr, dyfr, bfr] = FirstFootstepQPOptimizerFactory.makeFootReachability(params, ySep);
            [dxsp, dysp, bsp] = FirstFootstepQPOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
            
            jerkCost = FirstFootstepQPOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = FirstFootstepQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            zmpTrackCost = FirstFootstepQPOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma, m);
            
            cost = jerkCost + velTrackCost + zmpTrackCost;
            
            zmpErrorConst = FirstFootstepQPOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp, m);
            feetCollConst = FirstFootstepQPOptimizerFactory.makeFeetCollisionConstraints(variables, params, dxfr, dyfr, bfr);
            currStepConst = FirstFootstepQPOptimizerFactory.makeCurrentStepConstraints(variables, params);
            boundConst = FirstFootstepQPOptimizerFactory.makeBoundConstraints(variables);
            
            constraints = zmpErrorConst +...
                feetCollConst + currStepConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            times = zeros(1, 100);
            for i=1:100
                tic
                solution = problem(paramsValues);
                times(i) = toc;
            end
            
            figure;
            plot(times, 'LineWidth', 2);
            title(sprintf('mean %g std %g', mean(times), std(times)));
            xlabel('Iteration (-)', 'FontSize', 14);
            ylabel('Solver Time (s)', 'FontSize', 14);
            set(gca, 'FontSize', 14);
            
            d3X = solution{1};
            Xf = solution{2};
            d3Y = solution{3};
            Yf = solution{4};
            U = FirstFootstepQPOptimizerFactory.makeFootstepSelectMatrix(N, Nstep, m);
            Xfaux = U * Xf;
            Yfaux = U * Yf;
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xf, Yf, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testFullQP()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-4;
            N = 10;
            Nstep = 5;
            m = 5;
            bds = 0;
            if m == 5
                bds = 1;
            end
            Nf = FirstFootstepQPOptimizerFactory.getNumSteps(N, Nstep, m);
            delta = 10;
            T = 0.1;
            zCOM = 0.24;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            xc = 0;
            yc = 0;
            bls = 1;
            ySep = 0.4;
            xa = 0;
            ya = 0;
            axmax = 2;
            aymax = 2;
            maxAcceleration.axmax = axmax;
            maxAcceleration.aymax = aymax;
            maxVelocity.vxmax = 0.06;
            maxVelocity.vymax = 0.06;
            
            theta = zeros(Nf, 1);
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = 0.1 * ones(N, 1);
            dYr = zeros(N, 1);
            
            meanTimes = zeros(1, N);
            stdTimes = zeros(1, N);
            
            meanCplex = zeros(1, N);
            stdCplex = zeros(1, N);
            meanGurobi = zeros(1, N);
            stdGurobi = zeros(1, N);
            meanQuadprog = zeros(1, N);
            stdQuadprog = zeros(1, N);
            meanQpoases = zeros(1, N);
            stdQpoases = zeros(1, N);
            meanOsqp = zeros(1, N);
            stdOsqp = zeros(1, N);
            for m=1:N
                Nf = FirstFootstepQPOptimizerFactory.getNumSteps(N, Nstep, m);
                
                theta = zeros(Nf, 1);
                cf = cos(theta);
                sf = sin(theta);
                
                variables = FirstFootstepQPOptimizerFactory.makeVariables(N, Nstep, m);
                
                variablesCell = FirstFootstepQPOptimizerFactory.makeVariablesCell(variables);
                                
                params = FirstFootstepQPOptimizerFactory.makeParams(N, Nstep, m);
                
                paramsCell = FirstFootstepQPOptimizerFactory.makeParamsCell(params);
                
                paramsValues = {
                    dXr,...
                    dYr,...
                    x,...
                    y,...
                    xc,...
                    yc,...
                    xa,...
                    ya,...
                    bds,...
                    bls,...
                    cf,...
                    sf
                    };
                    
                [dxfr, dyfr, bfr] = FirstFootstepQPOptimizerFactory.makeFootReachability(params, ySep);
                [dxsp, dysp, bsp] = FirstFootstepQPOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
                [dxvl, dyvl, bvl] = FirstFootstepQPOptimizerFactory.makeVelocityLimits(params, maxVelocity.vxmax, maxVelocity.vymax);
                [dxa, dya, ba] = FirstFootstepQPOptimizerFactory.makeAccelerationLimits(params, maxAcceleration.axmax, maxAcceleration.aymax);

                jerkCost = FirstFootstepQPOptimizerFactory.makeJerkCost(variables, alpha);
                velTrackCost = FirstFootstepQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
                zmpTrackCost = FirstFootstepQPOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma, m);

                cost = jerkCost + velTrackCost + zmpTrackCost;
                
                zmpErrorConst = FirstFootstepQPOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp, m);
                feetCollConst = FirstFootstepQPOptimizerFactory.makeFeetCollisionConstraints(variables, params, dxfr, dyfr, bfr);
                jointsConst = FirstFootstepQPOptimizerFactory.makeJointsConstraints(variables, params, dxvl, dyvl, bvl, T, m);
                currStepConst = FirstFootstepQPOptimizerFactory.makeCurrentStepConstraints(variables, params);
                maxAccelConst = FirstFootstepQPOptimizerFactory.makeAccelerationConstraints(variables, params, prediction, dxa, dya, ba, m);
                boundConst = FirstFootstepQPOptimizerFactory.makeBoundConstraints(variables);

                constraints = zmpErrorConst + feetCollConst +...
                    jointsConst + currStepConst + maxAccelConst + boundConst;
                
                numCases = 100;

                options = sdpsettings('solver', 'quadprog', 'verbose', 0);

                problem = optimizer(constraints, cost, options, paramsCell, variablesCell);

                auxTimes = zeros(1, numCases);
                for i=1:numCases
                    tic;
                    solution = problem(paramsValues);
                    auxTimes(i) = toc;
                end
                meanQuadprog(m) = mean(auxTimes);
                stdQuadprog(m) = std(auxTimes);
                
                options = sdpsettings('solver', 'gurobi', 'verbose', 0);

                problem = optimizer(constraints, cost, options, paramsCell, variablesCell);

                auxTimes = zeros(1, numCases);
                for i=1:numCases
                    tic;
                    solution = problem(paramsValues);
                    auxTimes(i) = toc;
                end
                meanGurobi(m) = mean(auxTimes);
                stdGurobi(m) = std(auxTimes);
                
%                 options = sdpsettings('solver', 'qpoases');
% 
%                 problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
% 
%                 auxTimes = zeros(1, numCases);
%                 for i=1:numCases
%                     tic;
%                     solution = problem(paramsValues);
%                     auxTimes(i) = toc;
%                 end
%                 meanQpoases(m) = mean(auxTimes);
%                 stdQpoases(m) = std(auxTimes);
                
%                 options = sdpsettings('solver', 'osqp');
% 
%                 problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
% 
%                 auxTimes = zeros(1, numCases);
%                 for i=1:numCases
%                     tic;
%                     solution = problem(paramsValues);
%                     auxTimes(i) = toc;
%                 end
%                 meanOsqp(m) = mean(auxTimes);
%                 stdOsqp(m) = std(auxTimes);
            end
            
%             times.meanCplex = meanCplex;
%             times.stdCplex = stdCplex;
            times.meanGurobi = meanGurobi;
            times.stdGurobi = stdGurobi;
            times.meanQuadprog = meanQuadprog;
            times.stdQuadprog = stdQuadprog;
            times.meanQpoases = meanQpoases;
            times.stdQpoases = stdQpoases;
            times.meanOsqp = meanOsqp;
            times.stdOsqp = stdOsqp;
            
            save('times_quadprog_gurobi.mat');
            
%             meanTimes = times.meanGurobi;
%             stdTimes = times.stdGurobi;
            meanTimes = times.meanQpoases;
            stdTimes = times.stdQpoases;
            
            save('footstep_tester_qp.mat', 'times');
            
            figure;
            errorbar(meanTimes, 2 * stdTimes, 'LineWidth', 2);
            xlabel('Timesteps on first footstep (-)', 'FontSize', 14);
            ylabel('Solver Time (s)', 'FontSize', 14);
            set(gca, 'FontSize', 14);
            
            d3X = solution{1};
            Xf = solution{2};
            d3Y = solution{3};
            Yf = solution{4};
            U = FirstFootstepQPOptimizerFactory.makeFootstepSelectMatrix(N, Nstep, m);
            Xfaux = U * Xf;
            Yfaux = U * Yf;
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xf, Yf, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
    end
end
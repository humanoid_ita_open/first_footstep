function plotQPComputationalCost(times)

figure;
hold on;
% times.meanCplex = times.meanCplex * 10^3;
% times.stdCplex = times.stdCplex * 10^3;
% times.meanGurobi = times.meanGurobi * 10^3;
% times.stdGurobi = times.stdGurobi * 10^3;
% times.meanQuadprog = times.meanQuadprog * 10^3;
% times.stdQuadprog = times.stdQuadprog * 10^3;
% times.meanQpoases = times.meanQpoases * 10^3;
% times.stdQpoases = times.stdQpoases * 10^3;

times.meanGurobi = times.meanGurobi;
times.stdGurobi = times.stdGurobi;
times.meanQuadprog = times.meanQuadprog;
times.stdQuadprog = times.stdQuadprog;
times.meanQpoases = times.meanQpoases;
times.stdQpoases = times.stdQpoases;

grid on;

% errorbar(times.meanCplex, 2 * times.stdCplex, 'LineWidth', 2);
errorbar(times.meanGurobi, 2 * times.stdGurobi, 'LineWidth', 2);
errorbar(times.meanQuadprog, 2 * times.stdQuadprog, 'LineWidth', 2);
errorbar(times.meanQpoases, 2 * times.stdQpoases, 'LineWidth', 2);
legend({'Gurobi', 'quadprog', 'qpOASES'}, 'FontSize', 14, 'interpreter', 'latex');
set(gca, 'TickLabelInterpreter', 'latex');
set(gca, 'FontSize', 14);
xlabel('First footstep duration (timesteps)', 'FontSize', 14, 'interpreter', 'latex');
ylabel('Solver time ($s$)', 'FontSize', 14, 'interpreter', 'latex');
xlim([1, 10]);

end
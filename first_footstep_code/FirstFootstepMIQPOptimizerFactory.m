classdef FirstFootstepMIQPOptimizerFactory < handle
    properties
    end
    
    methods(Static)
        function variables = makeFirstFootstepVariables(N, Nstep)
            Nf = FirstFootstepMIQPOptimizerFactory.getMaxNf(N, Nstep);
            variables.d3X = sdpvar(N, 1);
            variables.Xfb = sdpvar(Nf, 1);
            variables.d3Y = sdpvar(N, 1);
            variables.Yfb = sdpvar(Nf, 1);
            variables.Xfaux = sdpvar(N, 1);
            variables.Yfaux = sdpvar(N, 1);
            variables.B = binvar(N, 1);
            variables.N = N;
            variables.Nstep = Nstep;
            variables.Nf = Nf;
        end
        
        function variablesCell = makeVariablesCell(variables)
            variablesCell = {
                variables.d3X,...   % 1
                variables.Xfb,...   % 2
                variables.d3Y,...   % 3
                variables.Yfb,...   % 4
                variables.Xfaux,... % 5
                variables.Yfaux,... % 6
                variables.B         % 7
                };
        end
        
        function params = makeFirstFootstepParams(N, Nstep)
            params.N = N;
            params.Nstep = Nstep;
            Nf = FirstFootstepMIQPOptimizerFactory.getMaxNf(N, Nstep);
            params.Nf = Nf;
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.kprime = sdpvar;
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            params.xc = sdpvar;
            params.yc = sdpvar;
            params.xa = sdpvar;
            params.ya = sdpvar;
            params.bds = sdpvar;
            params.bls = sdpvar;
            params.cf = sdpvar(N, Nf);
            params.sf = sdpvar(N, Nf);
        end
        
        function paramsCell = makeParamsCell(params)
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.kprime,...
                params.x,...
                params.y,...
                params.xc,...
                params.yc,...
                params.xa,...
                params.ya,...
                params.bds,...
                params.bls,...
                params.cf,...
                params.sf
                };
        end
        
        function Nf = getMaxNf(N, Nstep)
            Nf = 1 + ceil((N - 1) / Nstep);
        end
        
        function Nf = getNf(N, Nstep, m)
            Nf = 1 + ceil((N - m) / Nstep);
        end
        
        function cost = makeJerkCost(variables, alpha)
            cost = (alpha / 2) * variables.d3X' * variables.d3X +...
                (alpha / 2) * variables.d3Y' * variables.d3Y;
        end
        
        function cost = makeVelocityTrackingCost(variables, params, prediction, beta)
            velocityErrorX = params.dXr - (prediction.Pvs * params.x + prediction.Pvu * variables.d3X);
            velocityErrorY = params.dYr - (prediction.Pvs * params.y + prediction.Pvu * variables.d3Y);
            cost = (beta / 2) * (velocityErrorX' * velocityErrorX) +...
                (beta / 2) * (velocityErrorY' * velocityErrorY);
        end
        
        function cost = makeZMPTrackingCost(variables, params, prediction, gamma)
            zmpErrorX = variables.Xfaux - (prediction.Pzs * params.x + prediction.Pzu * variables.d3X);
            zmpErrorY = variables.Yfaux - (prediction.Pzs * params.y + prediction.Pzu * variables.d3Y);
            cost = (gamma / 2) * (zmpErrorX' * zmpErrorX) +...
                (gamma / 2) * (zmpErrorY' * zmpErrorY);
        end
        
        function cost = makeStepDurationCost(variables, params, delta)
            Sr1 = max(variables.Nstep - params.kprime, 0);
            S1 = FirstFootstepMIQPOptimizerFactory.computeS1(variables);
            cost = (delta / 2) * (S1 - Sr1)^2;
        end
        
        function [dxfr, dyfr, bfr] = makeFootReachability(params, ySep)
            dxfr = cell(params.N, params.Nf);
            dyfr = cell(params.N, params.Nf);
            
            for i=1:params.N
                for j=1:params.Nf
                    dxfr{i, j} = -params.sf(i, j);
                    dyfr{i, j} = params.cf(i, j);
                end
            end
            
            bfr = -ySep;
        end
        
        function [dxvl, dyvl, bvl] = makeVelocityLimits(params, vxmax, vymax)
            dxvl = cell(params.N, params.Nf);
            dyvl = cell(params.N, params.Nf);
            
            for i=1:params.N
                for j=1:params.Nf
                    dxvl{i, j} = [params.cf(i, j); -params.cf(i, j); -params.sf(i, j); params.sf(i, j)];
                    dyvl{i, j} = [params.sf(i, j); -params.sf(i, j); params.cf(i, j); -params.cf(i, j)];
                end
            end
            
            bvl = [vxmax; vxmax; vymax; vymax];
        end
        
        function [dxa, dya, ba] = makeAccelerationLimits(params, axmax, aymax)
            dxa = cell(params.N, params.Nf);
            dya = cell(params.N, params.Nf);
            
            for i=1:params.N
                for j=1:params.Nf
                    dxa{i, j} = [params.cf(i, j); -params.cf(i, j); -params.sf(i, j); params.sf(i, j)];
                    dya{i, j} = [params.sf(i, j); -params.sf(i, j); params.cf(i, j); -params.cf(i, j)];
                end
            end
            
            ba = [axmax; axmax; aymax; aymax];
        end
        
        function [dxsp, dysp, bsp] = makeSupportPolygon(params, footLength, footWidth)
            dxsp = cell(params.N, params.Nf);
            dysp = cell(params.N, params.Nf);
            
            for i=1:params.N
                for j=1:params.Nf
                    dxsp{i, j} = [params.cf(i, j); -params.cf(i, j); -params.sf(i, j); params.sf(i, j)];
                    dysp{i, j} = [params.sf(i, j); -params.sf(i, j); params.cf(i, j); -params.cf(i, j)];
                end
            end
            
            l = footLength;
            w = footWidth;
            bsp = [l/2; l/2; w/2; w/2];
        end
        
        function constraints = makeIntegerConstraints(variables)
            constraints = [sum(variables.B) == 1];
        end
        
        function constraints = makeAuxiliaryVariablesConstraints(variables)
            N = variables.N;
            Nstep = variables.Nstep;
            constraints = [];
            for m=1:N
                for i=1:N
                    index = FirstFootstepMIQPOptimizerFactory.timestepToFootstep(i, m, Nstep);
                    constraints = constraints +...
                        [implies(variables.B(m), [variables.Xfaux(i) == variables.Xfb(index)])];
                    constraints = constraints +...
                        [implies(variables.B(m), [variables.Yfaux(i) == variables.Yfb(index)])];
                end
            end
        end
        
        function constraints = makeZMPErrorConstraints(variables, params, prediction,...
                dxsp, dysp, bsp)
            N = variables.N;
            Nstep = variables.Nstep;
            constraints = [];
            Zx = prediction.Pzs * params.x + prediction.Pzu * variables.d3X;
            Zy = prediction.Pzs * params.y + prediction.Pzu * variables.d3Y;
            for m=1:N
                for i=1:N
                    index = FirstFootstepMIQPOptimizerFactory.timestepToFootstep(i, m, Nstep);
                    constraints = constraints +...
                        implies(variables.B(m),...
                        [dxsp{m, index}, dysp{m, index}] * [Zx(i) - variables.Xfaux(i); Zy(i) - variables.Yfaux(i)] <= bsp);
                end
            end
        end
        
        function constraints = makeFeetCollisionConstraints(variables, params,...
                dxfr, dyfr, bfr)
            N = variables.N;
            constraints = [];
            for m=1:N
                Nf = FirstFootstepMIQPOptimizerFactory.getNf(params.N, params.Nstep, m);
                for j=1:(Nf-1)
                    factor = (1 - 2 * params.bls) * (-1)^(j - 1); % had to compute without exp due to YALMIP optimizer limitations
                    constraints = constraints +...
                        implies(variables.B(m),...
                        factor * [dxfr{m, j}, dyfr{m, j}] * [variables.Xfb(j+1) - variables.Xfb(j); variables.Yfb(j+1) - variables.Yfb(j)] <= bfr);
                end
            end
        end
        
        function constraints = makeJointsConstraints(variables, params,...
                dxvl, dyvl, bvl, T)
            N = variables.N;
            Nf = variables.Nf;
            Nstep = variables.Nstep;
            S1 = FirstFootstepMIQPOptimizerFactory.computeS1(variables);
            constraints = [[dxvl{1, 1}, dyvl{1, 1}] * [variables.Xfb(2) - params.xa; variables.Yfb(2) - params.ya] <= (S1 - params.bds) * T * bvl];
            for m=1:N
                Nf = FirstFootstepMIQPOptimizerFactory.getNf(params.N, params.Nstep, m);
                for j=2:(Nf-1)
                    constraints = constraints +...
                        implies(variables.B(m), [dxvl{m, j}, dyvl{m, j}] * [variables.Xfb(j+1) - variables.Xfb(j-1); variables.Yfb(j+1) - variables.Yfb(j-1)] <= (Nstep - 1) * T * bvl);
                end
            end
        end
        
        function constraints = makeCurrentStepConstraints(variables, params)
            constraints = [variables.Xfb(1) == params.xc; variables.Yfb(1) == params.yc];
        end
        
        function constraints = makeAccelerationConstraints(variables, params, prediction,...
                dxa, dya, ba)
            N = variables.N;
            Nstep = variables.Nstep;
            d2X = prediction.Pas * params.x + prediction.Pau * variables.d3X;
            d2Y = prediction.Pas * params.y + prediction.Pau * variables.d3Y;
            constraints = [];
            for m=1:N
                for i=1:N
                    index = FirstFootstepMIQPOptimizerFactory.timestepToFootstep(i, m, Nstep);
                    constraints = constraints +...
                        implies(variables.B(m),...
                        [dxa{m, index}, dya{m, index}] * [d2X(i); d2Y(i)] <= ba);
                end
            end
        end
        
        function constraints = makeBoundConstraints(variables, params)
            N = variables.N;
            Nf = variables.Nf;
            constraints = [...
                -1000 * ones(N, 1) <= variables.d3X <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Xfb <= 10 * ones(Nf, 1),...
                -1000 * ones(N, 1) <= variables.d3Y <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Yfb <= 10 * ones(Nf, 1),...
                -10 * ones(N, 1) <= variables.Xfaux <= 10 * ones(N, 1),...
                -10 * ones(N, 1) <= variables.Yfaux <= 10 * ones(N, 1)];
            constraints = constraints +...
                [-1 * ones(N, 1) <= params.dXr <= 1 * ones(N, 1),...
                -1 * ones(N, 1) <= params.dYr <= 1 * ones(N, 1),...
                0 <= params.kprime <= N,...
                -100 * ones(3, 1) <= params.x <= 100 * ones(3, 1),...
                -100 * ones(3, 1) <= params.y <= 100 * ones(3, 1),...
                -10 <= params.xc <= 10,...
                -10 <= params.yc <= 10,...
                -10 <= params.xa <= 10,...
                -10 <= params.ya <= 10,...
                0 <= params.bds <= 1,...
                0 <= params.bls <= 1,...
                -1 * ones(N, Nf) <= params.cf <= 1 * ones(N, Nf),...
                -1 * ones(N, Nf) <= params.sf <= 1 * ones(N, Nf)];
        end
        
        function constraints = makeBoundConstraintsOnlyVariables(variables)
            N = variables.N;
            Nf = variables.Nf;
            constraints = [...
                -1000 * ones(N, 1) <= variables.d3X <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Xfb <= 10 * ones(Nf, 1),...
                -1000 * ones(N, 1) <= variables.d3Y <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Yfb <= 10 * ones(Nf, 1),...
                -10 * ones(N, 1) <= variables.Xfaux <= 10 * ones(N, 1),...
                -10 * ones(N, 1) <= variables.Yfaux <= 10 * ones(N, 1)];
        end
        
        function footstep = timestepToFootstep(timestep, m, Nstep)
            footstep = 1 + max(0, ceil((timestep - m) / Nstep));
        end
        
        function S1 = computeS1(variables)
            N = variables.N;
            S1 = (1:N) * variables.B;
        end
        
        function theta = planTheta(N, Nstep, dthetar, dthetaMax,...
                thetac, thetaa, kprime, T, bls, bds)
            Nf = FirstFootstepMIQPOptimizerFactory.getMaxNf(N, Nstep);
            theta = zeros(N, Nf);
            for m=1:N
                theta(m, 1) = thetac;
                for j=2:Nf
                    if ((bls && dthetar >= 0) || (~bls && dthetar < 0))
                        if j == 2
                            tmove = (m - bds) * T;
                            thetaPrev = thetaa;
                        else
                            tmove = (Nstep - 1) * T;
                            thetaPrev = theta(m, j - 1);
                        end
                        deltaThetaMax = dthetaMax * tmove;
                        thetad = thetac + dthetar * (kprime + m + Nstep) * T;
                        deltaTheta = saturate(thetad - thetaa, -deltaThetaMax, deltaThetaMax);
                        theta(m, j) = thetaPrev + deltaTheta;
                    else
                        theta(m, j) = theta(m, j - 1);
                    end
                end
            end
        end
    end
    
end
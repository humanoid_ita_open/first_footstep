function plotDisturbanceTolerance()

fontSize = 14;

fixed = load('simulation_many_fixed.mat');
automatic = load('simulation_many_automatic.mat');
first = load('simulation_many_first.mat');

amplitudes = fixed.amplitudes;
directions = fixed.directions * 180 / pi;

fixedY = zeros(1, length(directions));
firstY = zeros(1, length(directions));
automaticY = zeros(1, length(directions));

for d=1:length(directions)
    indices = find(fixed.results(:, d) > 0);
    fixedY(d) = amplitudes(indices(end));
    indices = find(automatic.results(:, d) > 0);
    automaticY(d) = amplitudes(indices(end));
    indices = find(first.results(:, d) > 0);
    firstY(d) = amplitudes(indices(end));
end

figure;
hold on;

plot(directions, fixedY, 'b', 'LineWidth', 2);
plot(directions, automaticY, 'r', 'LineWidth', 2);
plot(directions, firstY, 'g', 'LineWidth', 2);
xlabel('Direction ($^{\circ}$)', 'FontSize', fontSize, 'interpreter', 'latex');
ylabel('Amplitude ($m/s$)', 'FontSize', fontSize, 'interpreter', 'latex');
legend({'fixed', 'automatic', 'first'}, 'FontSize', fontSize, 'interpreter', 'latex');
set(gca, 'FontSize', fontSize);
set(gca, 'TickLabelInterpreter','latex');
xlim([0, 360]);
ylim([0, 0.4]);
grid on;

end
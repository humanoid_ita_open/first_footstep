classdef FirstFootstepQPOptimizerFactory < handle
    
    methods(Static)
        function variables = makeVariables(N, Nstep, m)
            Nf = FirstFootstepQPOptimizerFactory.getNumSteps(N, Nstep, m);
            variables.d3X = sdpvar(N, 1);
            variables.Xf = sdpvar(Nf, 1);
            variables.d3Y = sdpvar(N, 1);
            variables.Yf = sdpvar(Nf, 1);
            variables.N = N;
            variables.Nstep = Nstep;
            variables.Nf = Nf;
        end
        
        function variablesCell = makeVariablesCell(variables)
            variablesCell = {
                variables.d3X,... % 1
                variables.Xf,... % 2
                variables.d3Y,... % 3
                variables.Yf,... % 4
                };
        end
        
        function params = makeParams(N, Nstep, m)
            params.N = N;
            params.Nstep = Nstep;
            Nf = FirstFootstepQPOptimizerFactory.getNumSteps(N, Nstep, m);
            params.Nf = Nf;
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            params.xc = sdpvar;
            params.yc = sdpvar;
            params.xa = sdpvar;
            params.ya = sdpvar;
            params.bds = sdpvar;
            params.bls = sdpvar;
            params.cf = sdpvar(Nf, 1);
            params.sf = sdpvar(Nf, 1);
        end
        
        function paramsCell = makeParamsCell(params)
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.x,...
                params.y,...
                params.xc,...
                params.yc,...
                params.xa,...
                params.ya,...
                params.bds,...
                params.bls,...
                params.cf,...
                params.sf
                };
        end
        
        function cost = makeJerkCost(variables, alpha)
            cost = (alpha / 2) * variables.d3X' * variables.d3X +...
                (alpha / 2) * variables.d3Y' * variables.d3Y;
        end
        
        function cost = makeVelocityTrackingCost(variables, params, prediction, beta)
            velocityErrorX = params.dXr - (prediction.Pvs * params.x + prediction.Pvu * variables.d3X);
            velocityErrorY = params.dYr - (prediction.Pvs * params.y + prediction.Pvu * variables.d3Y);
            cost = (beta / 2) * (velocityErrorX' * velocityErrorX) +...
                (beta / 2) * (velocityErrorY' * velocityErrorY);
        end
        
        function cost = makeZMPTrackingCost(variables, params, prediction, gamma, m)
            U = FirstFootstepQPOptimizerFactory.makeFootstepSelectMatrix(variables.N, variables.Nstep, m);
            zmpErrorX = U * variables.Xf - (prediction.Pzs * params.x + prediction.Pzu * variables.d3X);
            zmpErrorY = U * variables.Yf - (prediction.Pzs * params.y + prediction.Pzu * variables.d3Y);
            cost = (gamma / 2) * (zmpErrorX' * zmpErrorX) +...
                (gamma / 2) * (zmpErrorY' * zmpErrorY);
        end
        
        function [dxfr, dyfr, bfr] = makeFootReachability(params, ySep)
            dxfr = cell(1, params.Nf);
            dyfr = cell(1, params.Nf);
            
            for i=1:params.Nf
                dxfr{i} = -params.sf(i);
                dyfr{i} = params.cf(i);
            end
            
            bfr = -ySep;
        end
        
        function [dxvl, dyvl, bvl] = makeVelocityLimits(params, vxmax, vymax)
            dxvl = cell(1, params.Nf);
            dyvl = cell(1, params.Nf);
            
            for i=1:params.Nf
                dxvl{i} = [params.cf(i); -params.cf(i); -params.sf(i); params.sf(i)];
                dyvl{i} = [params.sf(i); -params.sf(i); params.cf(i); -params.cf(i)];
            end
            
            bvl = [vxmax; vxmax; vymax; vymax];
        end
        
        function [dxa, dya, ba] = makeAccelerationLimits(params, axmax, aymax)
            dxa = cell(1, params.Nf);
            dya = cell(1, params.Nf);
            
            ba = [axmax; axmax; aymax; aymax];
            
            for i=1:params.Nf
                dxa{i} = [params.cf(i); -params.cf(i); -params.sf(i); params.sf(i)];
                dya{i} = [params.sf(i); -params.sf(i); params.cf(i); -params.cf(i)];
            end
        end
        
        function [dxsp, dysp, bsp] = makeSupportPolygon(params, footLength, footWidth)
            dxsp = cell(1, params.Nf);
            dysp = cell(1, params.Nf);
            
            for i=1:params.Nf
                dxsp{i} = [params.cf(i); -params.cf(i); -params.sf(i); params.sf(i)];
                dysp{i} = [params.sf(i); -params.sf(i); params.cf(i); -params.cf(i)];
            end
            
            l = footLength;
            w = footWidth;
            bsp = [l/2; l/2; w/2; w/2];
        end
        
        function constraints = makeZMPErrorConstraints(variables, params, prediction,...
                dxsp, dysp, bsp, m)
            N = variables.N;
            Nstep = variables.Nstep;
            constraints = [];
            Zx = prediction.Pzs * params.x + prediction.Pzu * variables.d3X;
            Zy = prediction.Pzs * params.y + prediction.Pzu * variables.d3Y;
            for i=1:N
                index = FirstFootstepQPOptimizerFactory.timestepToFootstep(i, m, Nstep);
                constraints = constraints +...
                    [[dxsp{index}, dysp{index}] * [Zx(i) - variables.Xf(index); Zy(i) - variables.Yf(index)] <= bsp];
            end
        end
        
        function constraints = makeFeetCollisionConstraints(variables, params,...
                dxfr, dyfr, bfr)
            Nf = variables.Nf;
            constraints = [];
            for j=1:(Nf-1)
                factor = (1 - 2 * params.bls) * (-1)^(j - 1); % had to compute without exp due to YALMIP optimizer limitations
                constraints = constraints +...
                    [factor * [dxfr{j}, dyfr{j}] * [variables.Xf(j+1) - variables.Xf(j); variables.Yf(j+1) - variables.Yf(j)] <= bfr];
            end
        end
        
        function constraints = makeJointsConstraints(variables, params,...
                dxvl, dyvl, bvl, T, m)
            Nf = variables.Nf;
            Nstep = variables.Nstep;
            constraints = [];
            if Nf > 1
                constraints = constraints + [[dxvl{1}, dyvl{1}] * [variables.Xf(2) - params.xa; variables.Yf(2) - params.ya] <= (m - params.bds) * T * bvl];
            end
            for j=2:(Nf-1)
                constraints = constraints + [[dxvl{j}, dyvl{j}] * [variables.Xf(j+1) - variables.Xf(j-1); variables.Yf(j+1) - variables.Yf(j-1)] <= (Nstep - 1) * T * bvl];
            end
        end
        
        function constraints = makeCurrentStepConstraints(variables, params)
            constraints = [variables.Xf(1) == params.xc; variables.Yf(1) == params.yc];
        end
        
        function constraints = makeAccelerationConstraints(variables, params, prediction,...
                dxa, dya, ba, m)
            N = variables.N;
            Nstep = variables.Nstep;
            d2X = prediction.Pas * params.x + prediction.Pau * variables.d3X;
            d2Y = prediction.Pas * params.y + prediction.Pau * variables.d3Y;
            constraints = [];
            for i=1:N
                index = FirstFootstepQPOptimizerFactory.timestepToFootstep(i, m, Nstep);
                constraints = constraints +...
                    [[dxa{index}, dya{index}] * [d2X(i); d2Y(i)] <= ba];
            end
        end
        
        function constraints = makeBoundConstraints(variables)
            N = variables.N;
            Nf = variables.Nf;
            constraints = [...
                -1000 * ones(N, 1) <= variables.d3X <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Xf <= 10 * ones(Nf, 1),...
                -1000 * ones(N, 1) <= variables.d3Y <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Yf <= 10 * ones(Nf, 1)];
        end
             
        function footstep = timestepToFootstep(timestep, m, Nstep)
            footstep = 1 + max(0, ceil((timestep - m) / Nstep));
        end
        
        function U = makeFootstepSelectMatrix(N, Nstep, m)
            Nf = FirstFootstepQPOptimizerFactory.getNumSteps(N, Nstep, m);
            U = zeros(N, Nf);
            for i=1:N
                j = FirstFootstepQPOptimizerFactory.timestepToFootstep(i, m, Nstep);
                U(i, j) = 1;
            end
        end
        
        function Nf = getNumSteps(N, Nstep, m)
            Nf = 1 + ceil((N - m) / Nstep);
        end
        
        function theta = planTheta(N, Nstep, dthetar, dthetaMax,...
                thetac, thetaa, kprime, T, bls, bds, m)
            Nf = FirstFootstepQPOptimizerFactory.getNumSteps(N, Nstep, m);
            theta = zeros(Nf, 1);
            theta(1) = thetac;
            for j=2:Nf
                if ((bls && dthetar >= 0) || (~bls && dthetar < 0))
                    if j == 2
                        tmove = (m - bds) * T;
                        thetaPrev = thetaa;
                    else
                        tmove = (Nstep - 1) * T;
                        thetaPrev = theta(j - 1);
                    end
                    deltaThetaMax = dthetaMax * tmove;
                    thetad = thetac + dthetar * (kprime + m + Nstep) * T;
                    deltaTheta = saturate(thetad - thetaa, -deltaThetaMax, deltaThetaMax);
                    theta(j) = thetaPrev + deltaTheta;
                else
                    theta(j) = theta(j - 1);
                end
            end
        end
    end
end
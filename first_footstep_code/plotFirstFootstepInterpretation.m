function plotFirstFootstepInterpretation()

% colors = linspecer(2);
colors = mangaspecer(2);

fontSize = 14;

x = 0:10;
y = [zeros(1, 3), 0.6*ones(1, 5), 1.1*ones(1, 3)];

figure;
hold on;
plot(0, 0, 'o', 'color', colors(1, :), 'LineWidth', 2);
plot(x(2:end), y(2:end), 'x', 'color', colors(2, :), 'LineWidth', 2);
grid on;
xlabel('Timestep (-)', 'FontSize', fontSize, 'interpreter', 'latex');
ylabel('$x_f$ ($m$)', 'FontSize', fontSize, 'interpreter', 'latex');
legend('Current Timestep', 'Prediction Horizon', 'FontSize', fontSize,...
'interpreter', 'latex', 'location', 'northoutside', 'orientation', 'horizontal');
set(gca, 'FontSize', fontSize);
set(gca, 'TickLabelInterpreter','latex')

print -depsc2 first_footstep_interpretation_a.eps

x = 0:10;
y = [zeros(1, 1), 0.6*ones(1, 5), 1.1*ones(1, 5)];

figure;
hold on;
plot(0, 0, 'o', 'color', colors(1, :), 'LineWidth', 2);
plot(x(2:end), y(2:end), 'x', 'color', colors(2, :), 'LineWidth', 2);
grid on;
xlabel('Timestep (-)', 'FontSize', fontSize, 'interpreter', 'latex');
ylabel('$x_f$ ($m$)', 'FontSize', fontSize, 'interpreter', 'latex');
legend('Current Timestep', 'Prediction Horizon', 'FontSize', fontSize,...
'interpreter', 'latex', 'location', 'northoutside', 'orientation', 'horizontal');
set(gca, 'FontSize', fontSize);
set(gca, 'TickLabelInterpreter','latex')

print -depsc2 first_footstep_interpretation_b.eps

end
function plotMIQPComputationalCost(times)

figure;
hold on;
plot(10^3 * times.timesCplex, 'LineWidth', 2);
plot(10^3 * times.timesGurobi, 'LineWidth', 2);
xlabel('Iteration (-)');
ylabel('Computational time (ms)');
legend('CPLEX', 'Gurobi');
set(gca, 'FontSize', 14);
grid on;

end
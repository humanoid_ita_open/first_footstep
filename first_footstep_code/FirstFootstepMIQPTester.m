classdef FirstFootstepMIQPTester < handle
    properties
    end
    
    methods(Static)
        function testTimestepToFootstep()
            N = 10;
            Nstep = 5;
            footsteps = zeros(1, N);
            for m=1:N
                for i=1:N
                    j = FirstFootstepMIQPOptimizerFactory.timestepToFootstep(i, m, Nstep);
                    footsteps(i) = j;
                end
                figure;
                hold on;
                title(sprintf('$m = %d$', m), 'FontSize', 14, 'interpreter', 'latex');
                plot(1:N, footsteps, 'LineWidth', 2);
                grid on;
                xlabel('Timestep', 'FontSize', 14, 'interpreter', 'latex');
                ylabel('Footstep', 'FontSize', 14, 'interpreter', 'latex');
                set(gca, 'FontSize', 14);
                set(gca, 'TickLabelInterpreter','latex')
            end
        end
        
        function testJerkCost()
            alpha = 10^-6;
            beta = 1;
            N = 10;
            kprime = 0;
            Nstep = 5;
            delta = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = ones(N, 1);
            dYr = ones(N, 1);
            
            variables = FirstFootstepMIQPOptimizerFactory.makeFirstFootstepVariables(N, Nstep);
            
            variablesCell = FirstFootstepMIQPOptimizerFactory.makeVariablesCell(variables);
            
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.kprime = sdpvar;
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.kprime,...
                params.x,...
                params.y
                };
            
            paramsValues = {
                dXr,...
                dYr,...
                kprime,...
                x,...
                y,...
                };
            
            jerkCost = FirstFootstepMIQPOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = FirstFootstepMIQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            stepDurCost = FirstFootstepMIQPOptimizerFactory.makeStepDurationCost(variables, params, delta);
            
            cost = jerkCost + velTrackCost + stepDurCost;
            
            constraints = [];
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            solution = problem(paramsValues);
            
            d3X = solution{1};
            Xfb = solution{2};
            d3Y = solution{3};
            Yfb = solution{4};
            Xfaux = solution{5};
            Yfaux = solution{6};
            
            theta = zeros(N, 1);
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testZMPSupportPolygon()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            kprime = 0;
            Nstep = 5;
            delta = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            xc = 0;
            yc = 0;
            
            Nf = FirstFootstepMIQPOptimizerFactory.getMaxNf(N, Nstep);
            
            theta = zeros(N, 1);
            cf = cos(zeros(N, Nf));
            sf = sin(zeros(N, Nf));
            
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = ones(N, 1);
            dYr = ones(N, 1);
            
            variables = FirstFootstepMIQPOptimizerFactory.makeFirstFootstepVariables(N, Nstep);
            
            variablesCell = FirstFootstepMIQPOptimizerFactory.makeVariablesCell(variables);
            
            params.N = N;
            params.Nstep = Nstep;
            params.Nf = FirstFootstepMIQPOptimizerFactory.getMaxNf(N, Nstep);
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.kprime = sdpvar;
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            params.xc = sdpvar;
            params.yc = sdpvar;
            params.cf = sdpvar(N, Nf);
            params.sf = sdpvar(N, Nf);
            
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.kprime,...
                params.x,...
                params.y,...
                params.xc,...
                params.yc,...
                params.cf,...
                params.sf
                };
            
            paramsValues = {
                dXr,...
                dYr,...
                kprime,...
                x,...
                y,...
                xc,...
                yc,...
                cf,...
                sf
                };
            
            [dxsp, dysp, bsp] = FirstFootstepMIQPOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
            
            jerkCost = FirstFootstepMIQPOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = FirstFootstepMIQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            zmpTrackCost = FirstFootstepMIQPOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma);
            stepDurCost = FirstFootstepMIQPOptimizerFactory.makeStepDurationCost(variables, params, delta);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost;
            
            integerConst = FirstFootstepMIQPOptimizerFactory.makeIntegerConstraints(variables);
            auxVarConst = FirstFootstepMIQPOptimizerFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = FirstFootstepMIQPOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp);
            currStepConst = FirstFootstepMIQPOptimizerFactory.makeCurrentStepConstraints(variables, params);
            boundConst = FirstFootstepMIQPOptimizerFactory.makeBoundConstraintsOnlyVariables(variables);
            
            constraints = integerConst + auxVarConst + zmpErrorConst +...
                currStepConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            solution = problem(paramsValues);
            
            d3X = solution{1};
            Xfb = solution{2};
            d3Y = solution{3};
            Yfb = solution{4};
            Xfaux = solution{5};
            Yfaux = solution{6};
            B = solution{7};
            B
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testZMPFeetCollision()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            Nstep = 5;
            Nf = FirstFootstepMIQPOptimizerFactory.getMaxNf(N, Nstep);
            kprime = 0;
            delta = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.6;
            footDimensions.width = 0.3;
            xc = 0;
            yc = 0;
            bls = 1;
            ySep = 0.4;
            
            theta = zeros(N, 1);
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = ones(N, 1);
            dYr = zeros(N, 1);
            cf = cos(zeros(N, Nf));
            sf = sin(zeros(N, Nf));
            
            variables = FirstFootstepMIQPOptimizerFactory.makeFirstFootstepVariables(N, Nstep);
            
            variablesCell = FirstFootstepMIQPOptimizerFactory.makeVariablesCell(variables);
            
            params.N = N;
            params.Nstep = Nstep;
            params.Nf = Nf;
            params.dXr = sdpvar(N, 1);
            params.dYr = sdpvar(N, 1);
            params.kprime = sdpvar;
            params.x = sdpvar(3, 1);
            params.y = sdpvar(3, 1);
            params.xc = sdpvar;
            params.yc = sdpvar;
            params.bls = sdpvar;
            params.cf = sdpvar(N, Nf);
            params.sf = sdpvar(N, Nf);
            
            paramsCell = {
                params.dXr,...
                params.dYr,...
                params.kprime,...
                params.x,...
                params.y,...
                params.xc,...
                params.yc,...
                params.bls,...
                params.cf,...
                params.sf
                };
            
            paramsValues = {
                dXr,...
                dYr,...
                kprime,...
                x,...
                y,...
                xc,...
                yc,...
                bls,...
                cf,...
                sf
                };
            
            [dxfr, dyfr, bfr] = FirstFootstepMIQPOptimizerFactory.makeFootReachability(params, ySep);
            [dxsp, dysp, bsp] = FirstFootstepMIQPOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
            
            jerkCost = FirstFootstepMIQPOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = FirstFootstepMIQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            zmpTrackCost = FirstFootstepMIQPOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma);
            stepDurCost = FirstFootstepMIQPOptimizerFactory.makeStepDurationCost(variables, params, delta);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost;
            
            integerConst = FirstFootstepMIQPOptimizerFactory.makeIntegerConstraints(variables);
            auxVarConst = FirstFootstepMIQPOptimizerFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = FirstFootstepMIQPOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp);
            feetCollConst = FirstFootstepMIQPOptimizerFactory.makeFeetCollisionConstraints(variables, params, dxfr, dyfr, bfr);
            currStepConst = FirstFootstepMIQPOptimizerFactory.makeCurrentStepConstraints(variables, params);
            boundConst = FirstFootstepMIQPOptimizerFactory.makeBoundConstraintsOnlyVariables(variables);
            
            constraints = integerConst + auxVarConst + zmpErrorConst +...
                feetCollConst + currStepConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi');
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            solution = problem(paramsValues);
            
            d3X = solution{1};
            Xfb = solution{2};
            d3Y = solution{3};
            Yfb = solution{4};
            Xfaux = solution{5};
            Yfaux = solution{6};
            B = solution{7};
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testFullMIQP()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            Nstep = 5;
            Nf = FirstFootstepMIQPOptimizerFactory.getMaxNf(N, Nstep);
            kprime = 1;
            delta = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.06;
            footDimensions.width = 0.03;
            xc = 0;
            yc = 0;
            bds = 1;
            bls = 1;
            ySep = 0.1;
            xa = 0;
            ya = ySep;
            axmax = 4;
            aymax = 4;
            vxmax = 0.6;
            vymax = 0.3;
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = 0.1 * ones(N, 1);
            dYr = zeros(N, 1);
            cf = cos(zeros(N, Nf));
            sf = sin(zeros(N, Nf));
            theta = zeros(N, 1);
            
            variables = FirstFootstepMIQPOptimizerFactory.makeFirstFootstepVariables(N, Nstep);
            
            variablesCell = FirstFootstepMIQPOptimizerFactory.makeVariablesCell(variables);
            
            params = FirstFootstepMIQPOptimizerFactory.makeFirstFootstepParams(N, Nstep);
            
            paramsCell = FirstFootstepMIQPOptimizerFactory.makeParamsCell(params);
            
            paramsValues = {
                dXr,...
                dYr,...
                kprime,...
                x,...
                y,...
                xc,...
                yc,...
                xa,...
                ya,...
                bds,...
                bls,...
                cf,...
                sf,...
                };
            
            [dxfr, dyfr, bfr] = FirstFootstepMIQPOptimizerFactory.makeFootReachability(params, ySep);
            [dxsp, dysp, bsp] = FirstFootstepMIQPOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
            [dxvl, dyvl, bvl] = FirstFootstepMIQPOptimizerFactory.makeVelocityLimits(params, vxmax, vymax);
            [dxa, dya, ba] = FirstFootstepMIQPOptimizerFactory.makeAccelerationLimits(params, axmax, aymax);
            
            jerkCost = FirstFootstepMIQPOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = FirstFootstepMIQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            zmpTrackCost = FirstFootstepMIQPOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma);
            stepDurCost = FirstFootstepMIQPOptimizerFactory.makeStepDurationCost(variables, params, delta);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost;
            
            integerConst = FirstFootstepMIQPOptimizerFactory.makeIntegerConstraints(variables);
            auxVarConst = FirstFootstepMIQPOptimizerFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = FirstFootstepMIQPOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp);
            feetCollConst = FirstFootstepMIQPOptimizerFactory.makeFeetCollisionConstraints(variables, params, dxfr, dyfr, bfr);
            jointsConst = FirstFootstepMIQPOptimizerFactory.makeJointsConstraints(variables, params, dxvl, dyvl, bvl, T);
            currStepConst = FirstFootstepMIQPOptimizerFactory.makeCurrentStepConstraints(variables, params);
            maxAccelConst = FirstFootstepMIQPOptimizerFactory.makeAccelerationConstraints(variables, params, prediction, dxa, dya, ba);
            boundConst = FirstFootstepMIQPOptimizerFactory.makeBoundConstraints(variables, params);
            
            constraints = integerConst + auxVarConst + zmpErrorConst + feetCollConst +...
                jointsConst + currStepConst + maxAccelConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi', 'verbose', 2);
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
            
            solution = problem(paramsValues);
            
            d3X = solution{1};
            Xfb = solution{2};
            d3Y = solution{3};
            Yfb = solution{4};
            Xfaux = solution{5};
            Yfaux = solution{6};
            B = solution{7};
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
        
        function testEvaluateComputeTime()
            alpha = 10^-6;
            beta = 1;
            gamma = 10^-6;
            N = 10;
            Nstep = 5;
            Nf = FirstFootstepMIQPOptimizerFactory.getMaxNf(N, Nstep);
            kprime = 1;
            delta = 100;
            T = 0.1;
            zCOM = 0.2;
            footDimensions = struct;
            footDimensions.length = 0.06;
            footDimensions.width = 0.03;
            xc = 0;
            yc = 0;
            bds = 1;
            bls = 1;
            ySep = 0.1;
            xa = 0;
            ya = ySep;
            axmax = 4;
            aymax = 4;
            vxmax = 0.6;
            vymax = 0.3;
            dtheta = -0.4;
            
            prediction = Prediction(LIPMDynamics(T, zCOM), N);
            
            x = [0; 0; 0];
            y = [0; 0; 0];
            dXr = 0.1 * ones(N, 1);
            dYr = zeros(N, 1);
            cf = cos(zeros(N, Nf));
            sf = sin(zeros(N, Nf));
            theta = zeros(N, 1);
            
            variables = FirstFootstepMIQPOptimizerFactory.makeFirstFootstepVariables(N, Nstep);
            
            variablesCell = FirstFootstepMIQPOptimizerFactory.makeVariablesCell(variables);
            
            params = FirstFootstepMIQPOptimizerFactory.makeFirstFootstepParams(N, Nstep);
            
            paramsCell = FirstFootstepMIQPOptimizerFactory.makeParamsCell(params);
            
            paramsValues = {
                dXr,...
                dYr,...
                kprime,...
                x,...
                y,...
                xc,...
                yc,...
                xa,...
                ya,...
                bds,...
                bls,...
                cf,...
                sf,...
                };
            
            [dxfr, dyfr, bfr] = FirstFootstepMIQPOptimizerFactory.makeFootReachability(params, ySep);
            [dxsp, dysp, bsp] = FirstFootstepMIQPOptimizerFactory.makeSupportPolygon(params, footDimensions.length, footDimensions.width);
            [dxvl, dyvl, bvl] = FirstFootstepMIQPOptimizerFactory.makeVelocityLimits(params, vxmax, vymax);
            [dxa, dya, ba] = FirstFootstepMIQPOptimizerFactory.makeAccelerationLimits(params, axmax, aymax);
            
            jerkCost = FirstFootstepMIQPOptimizerFactory.makeJerkCost(variables, alpha);
            velTrackCost = FirstFootstepMIQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, beta);
            zmpTrackCost = FirstFootstepMIQPOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, gamma);
            stepDurCost = FirstFootstepMIQPOptimizerFactory.makeStepDurationCost(variables, params, delta);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost;
            
            integerConst = FirstFootstepMIQPOptimizerFactory.makeIntegerConstraints(variables);
            auxVarConst = FirstFootstepMIQPOptimizerFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = FirstFootstepMIQPOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp);
            feetCollConst = FirstFootstepMIQPOptimizerFactory.makeFeetCollisionConstraints(variables, params, dxfr, dyfr, bfr);
            jointsConst = FirstFootstepMIQPOptimizerFactory.makeJointsConstraints(variables, params, dxvl, dyvl, bvl, T);
            currStepConst = FirstFootstepMIQPOptimizerFactory.makeCurrentStepConstraints(variables, params);
            maxAccelConst = FirstFootstepMIQPOptimizerFactory.makeAccelerationConstraints(variables, params, prediction, dxa, dya, ba);
            boundConst = FirstFootstepMIQPOptimizerFactory.makeBoundConstraints(variables, params);
            
            constraints = integerConst + auxVarConst + zmpErrorConst + feetCollConst +...
                jointsConst + currStepConst + maxAccelConst + boundConst;

            timesGurobi = zeros(1, 50);
            for i=1:50
                options = sdpsettings('solver', 'gurobi');
                problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
                tic;
                solution = problem(paramsValues);
                timesGurobi(i) = toc;
            end
            timesCplex = zeros(1, 50);
            for i=1:50
                options = sdpsettings('solver', 'gurobi');
                problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
                tic;
                solution = problem(paramsValues);
                timesCplex(i) = toc;
            end
            times.timesGurobi = timesGurobi;
            times.timesCplex = timesCplex;
            save('footstep_tester_miqp.mat', 'times');
            figure;
            plot(timesGurobi, 'LineWidth', 2);
            title(sprintf('mean %g std %g', mean(timesGurobi), std(timesGurobi)));
            xlabel('Iteration (-)', 'FontSize', 14);
            ylabel('Solver Time (s)', 'FontSize', 14);
            set(gca, 'FontSize', 14);
            
            d3X = solution{1};
            Xfb = solution{2};
            d3Y = solution{3};
            Yfb = solution{4};
            Xfaux = solution{5};
            Yfaux = solution{6};
            
            PlotterHumanoids.plotTest(d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux,...
                theta, prediction, x, y, T, N, footDimensions);
        end
    end
end
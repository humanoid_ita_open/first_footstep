classdef SimulationFirst < handle
    properties
        dynamics;
        controller;
        state;
        Ncon;
        kc;
        kd;
        xc;
        yc;
        thetac;
        xa;
        ya;
        isLeftSwing;
        d3x;
        d3y;
        xf1;
        yf1;
        theta1;
        dxa;
        dya;
        thetaa;
        dthetaa;
        isSwitchingStep;
        Ntouch;
        Xh;
        Yh;
        Xch;
        Ych;
        Thetach;
        Zxh;
        Zyh;
        d3Xh;
        d3Yh;
        dXh;
        dYh;
        d2Xh;
        d2Yh;
        xah;
        yah;
        thetaah;
        Xf1h;
        Yf1h;
        changingh;
        numFutureSteps;
        numFutureStepsh;
        feasible;
    end
    
    methods
        function self = SimulationFirst(dynamics, controller,...
            params)
            self.dynamics = dynamics;
            self.controller = controller;
            self.state = params.state0;
            self.xc = params.xc;
            self.yc = params.yc;
            self.thetac = params.thetac;
            self.xa = params.xa;
            self.ya = params.ya;
            self.thetaa = params.thetaa;
            self.isLeftSwing = params.isLeftSwing;
            self.Ncon = int32(self.controller.T / self.dynamics.T);
            self.kc = 0;
            self.kd = 1;
            self.isSwitchingStep = false;
            self.feasible = true;
        end
        
        function simulate(self, Nd, dxref, dyref, dthetaref, disturbances)
            self.Xh = zeros(Nd, 1);
            self.Yh = zeros(Nd, 1);
            self.Xch = zeros(Nd, 1);
            self.Ych = zeros(Nd, 1);
            self.Zxh = zeros(Nd, 1);
            self.Zyh = zeros(Nd, 1);
            self.Thetach = zeros(Nd, 1);
            self.xah = zeros(Nd, 1);
            self.yah = zeros(Nd, 1);
            self.d3Xh = zeros(Nd, 1);
            self.d3Yh = zeros(Nd, 1);
            self.dXh = zeros(Nd, 1);
            self.dYh = zeros(Nd, 1);
            self.d2Xh = zeros(Nd, 1);
            self.d2Yh = zeros(Nd, 1);
            self.xah = zeros(Nd, 1);
            self.yah = zeros(Nd, 1);
            self.thetaah = zeros(Nd, 1);
            self.Xf1h = zeros(Nd, 1);
            self.Yf1h = zeros(Nd, 1);
            self.changingh = zeros(Nd, 1);
            
            xDist = zeros(3, Nd);
            yDist = zeros(3, Nd);
            for d=1:length(disturbances)
                kDist = disturbances(d).kd;
                xDist(:, kDist) = disturbances(d).xDist;
                yDist(:, kDist) = disturbances(d).yDist;
            end
            
            for k=1:Nd
                if ~self.feasible
                    break;
                end
                self.step(dxref, dyref, dthetaref, xDist(:, k), yDist(:, k));
                
                x = self.state(1:3);
                y = self.state(4:6);
                self.Xh(k) = x(1);
                self.Yh(k) = y(1);
                self.Xch(k) = self.xc;
                self.Ych(k) = self.yc;
                self.Zxh(k) = self.dynamics.output(x);
                self.Zyh(k) = self.dynamics.output(y);
                self.Thetach(k) = self.thetac;
                self.d3Xh(k) = self.d3x;
                self.d3Yh(k) = self.d3y;
                self.dXh(k) = x(2);
                self.dYh(k) = y(2);
                self.d2Xh(k) = x(3);
                self.d2Yh(k) = y(3);
                self.numFutureStepsh(k) = self.numFutureSteps;
                self.xah(k) = self.xa;
                self.yah(k) = self.ya;
                self.thetaah(k) = self.thetaa;
                self.Xf1h(k) = self.xf1;
                self.Yf1h(k) = self.yf1;
                self.changingh(k) = self.isSwitchingStep;
            end
        end
        
        function step(self, dxr, dyr, dthetar, xDist, yDist)
            rotatedDistX = cos(self.thetac) * xDist - sin(self.thetac) * yDist;
            rotatedDistY = sin(self.thetac) * xDist + cos(self.thetac) * yDist;
            self.state = self.state + [rotatedDistX; rotatedDistY];
            x = self.state(1:3);
            y = self.state(4:6);
            
            if mod(self.kd, self.Ncon) == 1
                if self.isSwitchingStep == true
                    self.isSwitchingStep = false;
                end
                if self.Ntouch == 1
                    self.switchStep();
                end

                dXr = dxr * cos(self.thetac) * ones(self.controller.N, 1) - dyr * sin(self.thetac) * ones(self.controller.N, 1);
                dYr = dxr * sin(self.thetac) * ones(self.controller.N, 1) + dyr * cos(self.thetac) * ones(self.controller.N, 1);
                
                if strcmp(self.controller.type, 'first_miqp')
                    if self.isLeftSwing
                        bls = 1;
                    else
                        bls = 0;
                    end
                    [d3X, d3Y, Xfb, Yfb, ~, ~, B, theta, self.feasible] =...
                        self.controller.control(self.kc, x, y, dXr, dYr, dthetar,...
                            self.xc, self.yc, self.thetac, self.xa, self.ya, self.thetaa, bls);
                    self.Ntouch = (1:self.controller.N) * B;
                    self.numFutureSteps = FirstFootstepMIQPOptimizerFactory.getNf(self.controller.N, self.controller.Nstep, self.Ntouch);
                elseif strcmp(self.controller.type, 'first_qp') || strcmp(self.controller.type, 'fixed_duration')
                    if self.isLeftSwing
                        bls = 1;
                    else
                        bls = 0;
                    end
                    [d3X, d3Y, Xfb, Yfb, S1, theta, self.feasible] =...
                        self.controller.control(self.kc, x, y, dXr, dYr, dthetar,...
                            self.xc, self.yc, self.thetac, self.xa, self.ya, self.thetaa, bls);
                    self.Ntouch = S1;
                    self.numFutureSteps = FirstFootstepQPOptimizerFactory.getNumSteps(self.controller.N, self.controller.Nstep, S1);
                end
                
                self.d3x = d3X(1);
                self.d3y = d3Y(1);
                self.xf1 = Xfb(2);
                self.yf1 = Yfb(2);
                self.theta1 = theta(2);
                self.dxa = (self.xf1 - self.xa) / double(self.Ncon * self.Ntouch);
                self.dya = (self.yf1 - self.ya) / double(self.Ncon * self.Ntouch);
                self.dthetaa = (self.theta1 - self.thetaa) / double(self.Ncon * self.Ntouch);
            end
            
            x = self.dynamics.step(x, self.d3x);
            y = self.dynamics.step(y, self.d3y);
            
            self.state = [x; y];
            
            self.kd = self.kd + 1;
            if mod(self.kd, self.Ncon) == 1
                self.kc = self.kc + 1;
            end
            if ~self.isSwitchingStep
                self.xa = self.xa + self.dxa;
                self.ya = self.ya + self.dya;
                self.thetaa = self.thetaa + self.dthetaa;
            end
        end
        
        function switchStep(self)            
            self.kc = 0;
            self.isSwitchingStep = true;
            auxX = self.xa;
            auxY = self.ya;
            auxTheta = self.thetaa;
            self.xa = self.xc;
            self.ya = self.yc;
            self.thetaa = self.thetac;
            self.xc = auxX;
            self.yc = auxY;
            self.thetac = auxTheta;
            self.isLeftSwing = ~(self.isLeftSwing);
        end

    end
end
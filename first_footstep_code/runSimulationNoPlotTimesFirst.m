function processingTimes = runSimulationNoPlotTimesFirst()

yalmip('clear')

controller_type = 'first_miqp';
T = 0.1;
zCoM = 0.24;
footDimensions = struct;
footDimensions.length = 0.06;
footDimensions.width = 0.03;
vxmax = 0.6;
vymax = 0.6;
N = 10;
Nf = 10;
Nr = 11;
Nstep = 5;
alpha = 10^-6;
beta = 1;
gamma = 10^-4;
ySep = 0.1;
xc = 0;
yc = ySep;
thetac = 0;
x = [0; 0; 0];
y = [ySep; 0; 0];
xa = 0;
ya = 0;
dxref = 0.1;
% dxref = 0.0;
dyref = 0;
dtheta = 0.0;
% dtheta = -0.4;
isLeftSwing = false;
Ncon = 18;
Nprev = 5;
Nsim = N * Ncon * Nprev;
axmax = 2;
aymax = 2;
dthetaMax = 1.0;

simulationParams.state0 = [x; y];
simulationParams.xc = xc;
simulationParams.yc = yc;
simulationParams.thetac = thetac;
simulationParams.xa = xa;
simulationParams.ya = ya;
simulationParams.thetaa = 0;
simulationParams.isLeftSwing = isLeftSwing;

disturbances(1).kd = 2 * N * Ncon + 1;% - Nstep * Ncon; % used for showing the rotation heuristics
disturbances(1).time = disturbances(1).kd * T / Ncon;
% disturbances(1).xDist = [0; -0.06; 0];
% disturbances(1).xDist = [0; -0.19; 0];
% disturbances(1).xDist = [0; -0.1; 0];
disturbances(1).xDist = [0; 0.0; 0];
% disturbances(1).yDist = [0; 0.041; 0]; % right, not rotating
% disturbances(1).xDist = [0; -0.195; 0]; % very interesting, many steps to recover (rotating)
% disturbances(1).xDist = [0; -0.25; 0]; % very interesting, many steps to recover
% disturbances(1).xDist = [0; 0.0; 0];
% disturbances(1).xDist = [0; -0.15; 0];
% disturbances(1).yDist = [0; 0; 0];
disturbances(1).yDist = [0; 0.145; 0];
% disturbances(1).yDist = [0; 0.22; 0];
% disturbances(1).yDist = [0; 0.0; 0];
% disturbances(1).yDist = [0; 0.16; 0];
% disturbances(1).yDist = [0; -0.05; 0];

numCases = 100;

processingTimes = zeros(numCases, N * Nprev);

for i=1:numCases
    i
    delta = 10;
    if strcmp(controller_type, 'first_miqp')
        controller = FirstFootstepMIQPController(T, zCoM, N, Nstep, vxmax, vymax, axmax, aymax,...
            dthetaMax, footDimensions, ySep, alpha, beta, gamma, delta);
    elseif strcmp(controller_type, 'first_qp')
        controller = FirstFootstepQPController(T, zCoM, N, Nstep, vxmax, vymax, axmax, aymax,...
            dthetaMax, footDimensions, ySep, alpha, beta, gamma);
    elseif strcmp(controller_type, 'fixed_duration')
        controller = FixedDurationController(T, zCoM, N, Nstep, vxmax, vymax, axmax, aymax,...
            dthetaMax, footDimensions, ySep, alpha, beta, gamma);
    end

    dynamics = LIPMDynamics(T / Ncon, zCoM);

    simulation = SimulationFirst(dynamics, controller, simulationParams);

    simulation.simulate(Nsim, dxref, dyref, dtheta, disturbances);
    
    processingTimes(i, :) = simulation.controller.processingTimes;
end

meanTimes = mean(processingTimes, 1);
stdTimes = std(processingTimes, 1);

fontSize = 14;

figure;
errorbar(meanTimes, 2 * stdTimes, 'LineWidth', 2);
xlabel('Controller Timestep (-)', 'FontSize', fontSize, 'interpreter', 'latex');
ylabel('Solver Time (s)', 'FontSize', fontSize, 'interpreter', 'latex');
set(gca, 'TickLabelInterpreter','latex')
set(gca, 'FontSize', 14);
grid on;

totalMean = mean(reshape(processingTimes, size(processingTimes, 1) * size(processingTimes, 2), 1))
totalStd = std(reshape(processingTimes, size(processingTimes, 1) * size(processingTimes, 2), 1))

end
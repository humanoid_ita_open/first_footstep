classdef FirstFootstepQPController < handle
   properties
        type;
        T;
        zCoM;
        N;
        Nf;
        Nstep;
        maxVelocity;
        maxAcceleration;
        dthetaMax;
        footDimensions;
        ySep;
        alpha;
        beta;
        gamma;
        delta;
        switchingStep;
        solverTime;
        processingTimes;
        problems;
    end
    
    methods
        function self = FirstFootstepQPController(T, zCoM, N, Nstep,...
                vxmax, vymax, axmax, aymax, dthetaMax, footDimensions, ySep,...
                alpha, beta, gamma)
            self.type = 'first_qp';
            self.T = T;
            self.zCoM = zCoM;
            self.maxVelocity.vxmax = vxmax;
            self.maxVelocity.vymax = vymax;
            self.maxAcceleration.axmax = axmax;
            self.maxAcceleration.aymax = aymax;
            self.dthetaMax = dthetaMax;
            self.footDimensions = footDimensions;
            self.ySep = ySep;
            self.N = N;
            self.Nf = ceil((N - 1) / Nstep);
            self.Nstep = Nstep;
            self.alpha = alpha;
            self.beta = beta;
            self.gamma = gamma;
            self.processingTimes = [];
            self.problems = cell(1, N);
            self.makeOptimizationProblems();
        end
        
        function makeOptimizationProblems(self)
            self.problems = cell(1, self.N);
            
            prediction = Prediction(LIPMDynamics(self.T, self.zCoM), self.N);
            
            for m=1:self.N
                variables = FirstFootstepQPOptimizerFactory.makeVariables(self.N, self.Nstep, m);
                
                variablesCell = FirstFootstepQPOptimizerFactory.makeVariablesCell(variables);
                                
                params = FirstFootstepQPOptimizerFactory.makeParams(self.N, self.Nstep, m);
                
                paramsCell = FirstFootstepQPOptimizerFactory.makeParamsCell(params);
                    
                [dxfr, dyfr, bfr] = FirstFootstepQPOptimizerFactory.makeFootReachability(params, self.ySep);
                [dxsp, dysp, bsp] = FirstFootstepQPOptimizerFactory.makeSupportPolygon(params, self.footDimensions.length, self.footDimensions.width);
                [dxvl, dyvl, bvl] = FirstFootstepQPOptimizerFactory.makeVelocityLimits(params, self.maxVelocity.vxmax, self.maxVelocity.vymax);
                [dxa, dya, ba] = FirstFootstepQPOptimizerFactory.makeAccelerationLimits(params, self.maxAcceleration.axmax, self.maxAcceleration.aymax);

                jerkCost = FirstFootstepQPOptimizerFactory.makeJerkCost(variables, self.alpha);
                velTrackCost = FirstFootstepQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction, self.beta);
                zmpTrackCost = FirstFootstepQPOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, self.gamma, m);

                cost = jerkCost + velTrackCost + zmpTrackCost;
                
                zmpErrorConst = FirstFootstepQPOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction, dxsp, dysp, bsp, m);
                feetCollConst = FirstFootstepQPOptimizerFactory.makeFeetCollisionConstraints(variables, params, dxfr, dyfr, bfr);
                jointsConst = FirstFootstepQPOptimizerFactory.makeJointsConstraints(variables, params, dxvl, dyvl, bvl, self.T, m);
                currStepConst = FirstFootstepQPOptimizerFactory.makeCurrentStepConstraints(variables, params);
                maxAccelConst = FirstFootstepQPOptimizerFactory.makeAccelerationConstraints(variables, params, prediction, dxa, dya, ba, m);
                boundConst = FirstFootstepQPOptimizerFactory.makeBoundConstraints(variables);

                constraints = zmpErrorConst + feetCollConst +...
                    jointsConst + currStepConst + maxAccelConst + boundConst;
                
                options = sdpsettings('solver', 'gurobi');

                self.problems{m} = optimizer(constraints, cost, options, paramsCell, variablesCell);
            end
        end
        
        function [d3X, d3Y, Xfb, Yfb, S1, thetaf, feasible] = control(self, k, x, y,...
                dXr, dYr, dthetar, xc, yc, thetac, xa, ya, thetaa, bls)
            if k == 0
                bds = 1;
            else
                bds = 0;
            end
            
            S1r = max(0, self.Nstep - k);
            minimumStepCost = self.N^2;
            
            processingTime = 0;
            self.solverTime = 0;
            for m=1:self.N
                theta = FirstFootstepQPOptimizerFactory.planTheta(self.N, self.Nstep, dthetar, self.dthetaMax,...
                    thetac, thetaa, k, self.T, bls, bds, m);
                
                cf = cos(theta);
                sf = sin(theta);
                
                paramsValues = {
                    dXr,...
                    dYr,...
                    x,...
                    y,...
                    xc,...
                    yc,...
                    xa,...
                    ya,...
                    bds,...
                    bls,...
                    cf,...
                    sf
                    };
                
                tic;
                [solution, error] = self.problems{m}(paramsValues);
                processingTime = processingTime + toc;
                
                stepCost = (m - S1r)^2;
                if (error == 0) && (stepCost <= minimumStepCost)
                    minimumStepCost = stepCost;
                    d3X = solution{1};
                    Xfb = solution{2};
                    d3Y = solution{3};
                    Yfb = solution{4};
                    S1 = m;
                    thetaf = theta;
                    feasible = true;
                end
            end
            self.processingTimes(end + 1) = processingTime;
        end
        
        function numSteps = getNumSteps(self, k)
            numSteps = self.Nf;
        end
    end
end
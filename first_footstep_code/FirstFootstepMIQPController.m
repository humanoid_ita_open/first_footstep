classdef FirstFootstepMIQPController < handle
    properties
        type;
        T;
        zCoM;
        N;
        Nf;
        Nstep;
        maxVelocity;
        maxAcceleration;
        dthetaMax;
        footDimensions;
        ySep;
        alpha;
        beta;
        gamma;
        delta;
        switchingStep;
        processingTimes;
        problem;
    end
    
    methods
        function self = FirstFootstepMIQPController(T, zCoM, N, Nstep,...
                vxmax, vymax, axmax, aymax, dthetaMax, footDimensions, ySep,...
                alpha, beta, gamma, delta)
            self.type = 'first_miqp';
            self.T = T;
            self.zCoM = zCoM;
            self.maxVelocity.vxmax = vxmax;
            self.maxVelocity.vymax = vymax;
            self.maxAcceleration.axmax = axmax;
            self.maxAcceleration.aymax = aymax;
            self.dthetaMax = dthetaMax;
            self.footDimensions = footDimensions;
            self.ySep = ySep;
            self.N = N;
            self.Nf = FirstFootstepMIQPOptimizerFactory.getMaxNf(N, Nstep);
            self.Nstep = Nstep;
            self.alpha = alpha;
            self.beta = beta;
            self.gamma = gamma;
            self.delta = delta;
            self.processingTimes = [];
            self.problem = self.makeOptimizationProblem();
        end
        
        function problem = makeOptimizationProblem(self)
            variables = FirstFootstepMIQPOptimizerFactory.makeFirstFootstepVariables(self.N, self.Nstep);
            
            variablesCell = FirstFootstepMIQPOptimizerFactory.makeVariablesCell(variables);
            
            params = FirstFootstepMIQPOptimizerFactory.makeFirstFootstepParams(self.N, self.Nstep);
            
            paramsCell = FirstFootstepMIQPOptimizerFactory.makeParamsCell(params);
            
            [dxsp, dysp, bsp] = FirstFootstepMIQPOptimizerFactory.makeSupportPolygon(params, self.footDimensions.length, self.footDimensions.width);
            [dxfr, dyfr, bfr] = FirstFootstepMIQPOptimizerFactory.makeFootReachability(params, self.ySep);
            [dxvl, dyvl, bvl] = FirstFootstepMIQPOptimizerFactory.makeVelocityLimits(params, self.maxVelocity.vxmax, self.maxVelocity.vymax);
            [dxa, dya, ba] = FirstFootstepMIQPOptimizerFactory.makeAccelerationLimits(params, self.maxAcceleration.axmax, self.maxAcceleration.aymax);
            
            prediction = Prediction(LIPMDynamics(self.T, self.zCoM), self.N);
            
            jerkCost = FirstFootstepMIQPOptimizerFactory.makeJerkCost(variables, self.alpha);
            velTrackCost = FirstFootstepMIQPOptimizerFactory.makeVelocityTrackingCost(variables, params, prediction,...
                self.beta);
            zmpTrackCost = FirstFootstepMIQPOptimizerFactory.makeZMPTrackingCost(variables, params, prediction, self.gamma);
            stepDurCost = FirstFootstepMIQPOptimizerFactory.makeStepDurationCost(variables, params, self.delta);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost;
            
            integerConst = FirstFootstepMIQPOptimizerFactory.makeIntegerConstraints(variables);
            auxVarConst = FirstFootstepMIQPOptimizerFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = FirstFootstepMIQPOptimizerFactory.makeZMPErrorConstraints(variables, params, prediction,...
                dxsp, dysp, bsp);
            feetCollConst = FirstFootstepMIQPOptimizerFactory.makeFeetCollisionConstraints(variables, params,...
                dxfr, dyfr, bfr);
            jointsConst = FirstFootstepMIQPOptimizerFactory.makeJointsConstraints(variables, params,...
                dxvl, dyvl, bvl, self.T);
            currStepConst = FirstFootstepMIQPOptimizerFactory.makeCurrentStepConstraints(variables, params);
            maxAccelConst = FirstFootstepMIQPOptimizerFactory.makeAccelerationConstraints(variables, params,...
                prediction, dxa, dya, ba);
            boundConst = FirstFootstepMIQPOptimizerFactory.makeBoundConstraints(variables, params);
            
            constraints = integerConst + auxVarConst + zmpErrorConst + feetCollConst +...
                jointsConst + currStepConst + maxAccelConst + boundConst;
            
            options = sdpsettings('solver', 'gurobi', 'verbose', 0);
            
            problem = optimizer(constraints, cost, options, paramsCell, variablesCell);
        end
        
        function [d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, B, theta, feasible] = control(self, k, x, y,...
                dXr, dYr, dthetar, xc, yc, thetac, xa, ya, thetaa, bls)
            if k == 0
                bds = 1;
            else
                bds = 0;
            end
            
            theta = FirstFootstepMIQPOptimizerFactory.planTheta(self.N, self.Nstep, dthetar, self.dthetaMax,...
                thetac, thetaa, k, self.T, bls, bds);
            
            cf = cos(theta);
            sf = sin(theta);
            
            paramsValues = {
                dXr,...
                dYr,...
                k,...
                x,...
                y,...
                xc,...
                yc,...
                xa,...
                ya,...
                bds,...
                bls,...
                cf,...
                sf
                };
            
            tic;
            [solution, error] = self.problem(paramsValues);
            self.processingTimes(end + 1) = toc;
            
            d3X = solution{1};
            Xfb = solution{2};
            d3Y = solution{3};
            Yfb = solution{4};
            Xfaux = solution{5};
            Yfaux = solution{6};
            B = solution{7};
            
            feasible = (error == 0);
            
            if feasible
                m = (1:self.N) * B;
                theta = theta(m, :);
            else
                theta = theta(1, :);
            end
        end
        
        function numSteps = getNumSteps(self, k)
            numSteps = self.Nf;
        end
    end
end
function plotRealData()

data = load('data.txt');

iteration = data(:, 1);
time = data(:, 2);

idx = find(time >= 22.68 & time <= (22.68 + 7));

iteration = data(idx, 1);
time = data(idx, 2);
time = time - time(1);
x = data(idx, 3);
y = data(idx, 4);
psi = data(idx, 5);
x2D = data(idx, 6);
y2D = data(idx, 7);

psi = movmean(psi, 30);

xRot = x .* cos(psi) - y .* sin(psi);
yRot = x .* sin(psi) + y .* cos(psi);

figure;
plot(xRot, yRot);
axis equal;
figure;
plot(time, xRot);
figure;
plot(time, yRot);
figure;
plot(time, psi);
figure;
plot(x2D);
figure;
plot(y2D);
vx = differentiate(time, xRot);
vy = differentiate(time, yRot);
figure;
plot(time, movmean(-vx, 5), 'LineWidth', 2);
hold on;
plot(time, movmean(-vx, 50), 'g--', 'LineWidth', 2);
plot([time(1), time(end)], [0.1, 0.1], 'r--', 'LineWidth', 2);
plot([3.5, 3.5], [min(-vx), max(-vx)], 'k--', 'LineWidth', 2);
xlabel('Time ($s$)', 'FontSize', 14, 'interpreter', 'latex');
ylabel('X CoM Speed ($m/s$)', 'FontSize', 14, 'interpreter', 'latex');
set(gca, 'FontSize', 14);
set(gca, 'ticklabelinterpreter', 'latex');
xlim([time(1), time(end)]);
grid on;
figure;
plot(time, movmean(-vy, 5), 'LineWidth', 2);
hold on;
plot([3.5, 3.5], [min(vy), max(vy)], 'k--', 'LineWidth', 2);
xlabel('Time ($s$)', 'FontSize', 14, 'interpreter', 'latex');
ylabel('Y CoM Speed ($m/s$)', 'FontSize', 14, 'interpreter', 'latex');
set(gca, 'FontSize', 14);
set(gca, 'ticklabelinterpreter', 'latex');
xlim([time(1), time(end)]);
grid on;

end

function diff = differentiate(time, value)

diff = zeros(length(value), 1);

diff(1) = (value(2) - value(1)) / (time(2) - time(1));
diff(end) = (value(end) - value(end-1)) / (time(end) - time(end-1));

diff(2:end-1) = (value(3:end) - value(1:end-2)) ./ (time(3:end) - time(1:end-2));

end